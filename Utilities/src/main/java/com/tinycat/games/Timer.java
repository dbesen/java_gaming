package com.tinycat.games;

import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

public class Timer {

	private static Map<String, Long> startTimes = Maps.newHashMap();
	static Map<String, Long> finishedTimes = Maps.newHashMap();

	public static void start(final String timerName) {
		finishedTimes.remove(timerName);
		startTimes.put(timerName, System.nanoTime());
	}

	public static long stop(final String timerName) {
		final long time = System.nanoTime();
		Preconditions.checkArgument(startTimes.containsKey(timerName));
		final long elapsed = time - startTimes.get(timerName);
		finishedTimes.put(timerName, elapsed);
		startTimes.remove(timerName);
		return elapsed;
	}

	public static long getTimeNanoseconds(final String timerName) {
		final long time = System.nanoTime();
		if (finishedTimes.containsKey(timerName)) {
			return finishedTimes.get(timerName);
		}
		Preconditions.checkArgument(startTimes.containsKey(timerName));
		return time - startTimes.get(timerName);
	}

}
