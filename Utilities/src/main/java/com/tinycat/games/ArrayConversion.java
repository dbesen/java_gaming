package com.tinycat.games;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

public class ArrayConversion {

	public static Collection<Integer> convertToCollection(final int[] setting) {
		final List<Integer> toReturn = Lists.newArrayListWithCapacity(setting.length);
		for (final int element : setting) {
			toReturn.add(element);
		}
		return toReturn;
	}

	public static int[] convertToArray(final Collection<Integer> values) {
		final int[] toReturn = new int[values.size()];
		final Iterator<Integer> valueIterator = values.iterator();
		for (int i = 0; i < toReturn.length; i++) {
			toReturn[i] = valueIterator.next();
		}
		return toReturn;
	}

}
