package com.tinycat.games;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Iterator;

public class Asserts {

	public static <T> void assertCollectionEquals(final Collection<T> actual,
			final T... expected) {
		assertEquals(expected.length, actual.size());
		final Iterator<T> iterator = actual.iterator();
		for (final T item : expected)
			assertEquals(item, iterator.next());
	}

	public static void assertNotEquals(final double a, final double b,
			final double delta) {
		try {
			assertEquals("a is " + a + ", b is " + b, a, b, delta);
		} catch (final AssertionError e) {
			// Pass
			return;
		}
		fail("Expected an AssertionError; a is " + a + ", b is " + b);
	}

	public static void assertNotEquals(final short[] a, final short[] b) {
		try {
			assertArrayEquals(a, b);
		} catch (final AssertionError e) {
			// Pass
			return;
		}
		fail("Expected an AssertionError");
	}
}
