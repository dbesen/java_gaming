package com.tinycat.games;

public class MathUtils {

	/**
	 * From http://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/
	 * 
	 * Supposedly about 23 times faster than Math.pow()
	 */
	public static double approximatePow(final double a, final double b) {
		final long tmp = Double.doubleToLongBits(a);
		final long tmp2 = (long) (b * (tmp - 4606921280493453312L)) + 4606921280493453312L;
		return Double.longBitsToDouble(tmp2);
	}

	/**
	 * From
	 * http://stackoverflow.com/questions/101439/the-most-efficient-way-to-implement-an-integer-based-power-function-powint-int
	 */
	public static long ipow(long base, long exp) {
		long result = 1;
		while (exp != 0) {
			if (Long.lowestOneBit(exp) == 1)
				result *= base;
			exp >>= 1;
			base *= base;
		}

		return result;
	}

	public static long fastFloor(final double x) {
		return x < 0 ? (long) (x - 0.99999999999d) : (long) x;
		// return DoubleMath.roundToLong(x, RoundingMode.FLOOR); // too slow
	}

	// todo: cosine interpolation, cubic interpolation
	public static double LinearInterpolate(final double a, final double b, final double x) {
		return ((b - a) * x) + a;
	}
}
