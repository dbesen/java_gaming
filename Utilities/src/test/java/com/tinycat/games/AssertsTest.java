package com.tinycat.games;

import static com.tinycat.games.Asserts.assertNotEquals;

import org.junit.Test;

public class AssertsTest {

	@Test
	public void testNotEqualsPass() {
		assertNotEquals(2.3, 2.4, 0.0d);
		assertNotEquals(0.1 + 0.7, 0.8, 0.0d);
		assertNotEquals(0.1, 0.7, 0.2);
	}

	@Test(expected = AssertionError.class)
	public void testNotEqualsFailZero() throws Exception {
		assertNotEquals(0, 0, 0);
	}

	@Test(expected = AssertionError.class)
	public void testNotEqualsFailWithDelta() throws Exception {
		assertNotEquals(0, 0.1, 0.2);
	}

	@Test
	public void testAssertArrayNotEqualsPass() throws Exception {
		assertNotEquals(null, new short[] {});
		assertNotEquals(new short[] {}, null);
		assertNotEquals(new short[] {}, new short[] { 1 });
		assertNotEquals(new short[] { 1 }, new short[] {});
		assertNotEquals(new short[] { 3 }, new short[] { 4 });
		assertNotEquals(new short[] { 3 }, new short[] { 4, 5 });
		assertNotEquals(new short[] { 5, 6 }, new short[] { 5, 7 });
		assertNotEquals(new short[] { 5, 6 }, new short[] { 6, 6 });
	}

	@Test(expected = AssertionError.class)
	public void testAssertArrayNotEqualsThrowsNull() throws Exception {
		assertNotEquals((short[]) null, (short[]) null);
	}

	@Test(expected = AssertionError.class)
	public void testAssertArrayNotEqualsThrowsEmpty() throws Exception {
		assertNotEquals(new short[] {}, new short[] {});
	}

	@Test(expected = AssertionError.class)
	public void testAssertArrayNotEqualsThrowsOne() throws Exception {
		assertNotEquals(new short[] { 1 }, new short[] { 1 });
	}

	@Test(expected = AssertionError.class)
	public void testAssertArrayNotEqualsThrowsTwo() throws Exception {
		assertNotEquals(new short[] { 1, 2 }, new short[] { 1, 2 });
	}

}
