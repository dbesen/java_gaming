package com.tinycat.games;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MathUtilsTest {

	@Test
	public void testApproximatePow() {
		assertEquals(4, MathUtils.approximatePow(2, 2), 0.3);
	}

	@Test
	public void testipow() throws Exception {
		assertEquals(4, MathUtils.ipow(2, 2));
		assertEquals(1, MathUtils.ipow(0, 0));
		assertEquals(8, MathUtils.ipow(2, 3));
		assertEquals(1, MathUtils.ipow(1, 150));
		assertEquals(64, MathUtils.ipow(8, 2));
	}

	@Test
	public void testFastFloor() throws Exception {
		assertEquals(0, MathUtils.fastFloor(0));
		assertEquals(1, MathUtils.fastFloor(1));
		assertEquals(-1, MathUtils.fastFloor(-1));
		assertEquals(100000, MathUtils.fastFloor(100000));
		assertEquals(-100000, MathUtils.fastFloor(-100000));

		assertEquals(0, MathUtils.fastFloor(0.00000000001d));
		assertEquals(-1, MathUtils.fastFloor(-0.00000000001d));

		assertEquals(1, MathUtils.fastFloor(1.5));
		assertEquals(-2, MathUtils.fastFloor(-1.5));
	}

	@Test
	public void testLinearInterpolation() {
		assertEquals(0, MathUtils.LinearInterpolate(0, 1, 0), 0);
		assertEquals(1, MathUtils.LinearInterpolate(0, 1, 1), 0);
		assertEquals(.5, MathUtils.LinearInterpolate(0, 1, .5), 0);
		assertEquals(1.5, MathUtils.LinearInterpolate(1, 2, .5), 0);
		assertEquals(1, MathUtils.LinearInterpolate(0, 4, .25), 0);
		assertEquals(-1, MathUtils.LinearInterpolate(0, -4, .25), 0);
	}
}
