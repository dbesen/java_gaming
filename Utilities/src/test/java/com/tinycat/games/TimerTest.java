package com.tinycat.games;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TimerTest {
	@Test
	public void testTimer() {
		Timer.start("timer");
		final long time = Timer.stop("timer");
		assertEquals(time, Timer.getTimeNanoseconds("timer"));
		assertEquals(time, Timer.getTimeNanoseconds("timer"));
	}

	@Test
	public void testTwoTimers() {
		Timer.start("timer1");
		Timer.start("timer2");
		final long time2 = Timer.stop("timer2");
		assertEquals(time2, Timer.getTimeNanoseconds("timer2"));
		final long time1 = Timer.stop("timer1");
		assertEquals(time1, Timer.getTimeNanoseconds("timer1"));
		assertTrue(time1 >= time2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStopWithoutStart() {
		Timer.stop("timer");
	}

	@Test
	public void testGetWithoutStop() {
		Timer.start("timer");
		final long time = Timer.getTimeNanoseconds("timer");
		final long time2 = Timer.stop("timer");
		assertEquals(time2, Timer.getTimeNanoseconds("timer"));
		assertTrue(time2 >= time);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetWithoutStart() {
		Timer.getTimeNanoseconds("timer3");
	}
}
