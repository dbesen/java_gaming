package com.tinycat.games;

import static org.junit.Assert.assertArrayEquals;

import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

public class ArrayConversionTest {

	@Test
	public void testConvertToArray() {
		final List<Integer> intList = Lists.newArrayList(3, 4, 5);
		final int[] converted = ArrayConversion.convertToArray(intList);
		assertArrayEquals(new int[] { 3, 4, 5 }, converted);
	}

	@Test
	public void testConvertToArrayEmpty() {
		final List<Integer> intList = Lists.newArrayList();
		final int[] converted = ArrayConversion.convertToArray(intList);
		assertArrayEquals(new int[0], converted);
	}

	@Test(expected = NullPointerException.class)
	public void testConvertToArrayNull() {
		ArrayConversion.convertToArray(null);
	}

	@Test
	public void testConvertFromArray() {
		final int[] intArray = { 3, 4, 5 };
		final Collection<Integer> converted = ArrayConversion.convertToCollection(intArray);
		Asserts.assertCollectionEquals(converted, 3, 4, 5);
	}

	@Test(expected = NullPointerException.class)
	public void testConvertFromArrayNull() {
		ArrayConversion.convertToCollection(null);
	}

	@Test
	public void testConvertFromArrayEmpty() {
		final Collection<Integer> converted = ArrayConversion.convertToCollection(new int[0]);
		Asserts.assertCollectionEquals(converted);
	}
}
