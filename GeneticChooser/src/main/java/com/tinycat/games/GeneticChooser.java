package com.tinycat.games;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

public class GeneticChooser {

	private final Map<String, Mutatable<?>> parents = Maps.newHashMap();
	private final Map<String, Mutatable<?>> children = Maps.newHashMap();
	private double mutationFactor = 1;

	public void nextChild() {
		children.clear();
		for (final Entry<String, Mutatable<?>> entry : parents.entrySet()) {
			final String name = entry.getKey();
			final Mutatable<?> child = entry.getValue();
			final Mutatable<?> newChild = child.mutate(mutationFactor);
			children.put(name, newChild);
		}
	}

	public void breedCurrentChild() {
		parents.clear();
		parents.putAll(children);
		mutationFactor /= 2;
	}

	public void add(final String name, final Mutatable<?> child) {
		parents.put(name, child);
		children.put(name, child);
	}

	// We want a runtime exception for a type casting error here, so we just suppress the warning.
	@SuppressWarnings("unchecked")
	public <T extends Mutatable<?>> T get(final String name) {
		return (T) children.get(name);
	}

	public double getMutationFactor() {
		return mutationFactor;
	}

}
