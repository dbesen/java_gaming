package com.tinycat.games;

public interface Mutatable<T extends Mutatable<T>> {
	public T mutate(final double mutationFactor);
}
