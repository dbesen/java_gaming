package com.tinycat.games;

/**
 * Even though this class is named Mutatable, it is immutable.
 */
public class MutatableColor implements Mutatable<MutatableColor> {

	private final double red, green, blue;

	public MutatableColor() {
		red = 0;
		green = 0;
		blue = 0;
	}

	private MutatableColor(final double red, final double green, final double blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public double getRed() {
		return red;
	}

	public double getGreen() {
		return green;
	}

	public double getBlue() {
		return blue;
	}

	@Override
	public MutatableColor mutate(final double mutationFactor) {
		return new MutatableColor(mutateOne(red, mutationFactor), mutateOne(green, mutationFactor), mutateOne(blue, mutationFactor));
	}

	private static double mutateOne(final double color, final double mutationFactor) {
		return mutateByPoint(color, Math.random(), mutationFactor);
	}

	static double mutateByPoint(final double color, final double mutationPoint, final double mutationFactor) {
		return (mutationFactor * (mutationPoint - color)) + color;
	}

}
