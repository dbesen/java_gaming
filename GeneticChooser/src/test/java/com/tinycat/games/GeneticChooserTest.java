package com.tinycat.games;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GeneticChooserTest {
	@Test
	public void testNoExceptions() {
		final GeneticChooser chooser = new GeneticChooser();
		chooser.add("color", new MutatableColor());
		chooser.nextChild();
		chooser.nextChild();
		chooser.nextChild();
		chooser.breedCurrentChild();
		chooser.nextChild();
		chooser.nextChild();
		chooser.nextChild();
		chooser.get("color");
	}

	@Test
	public void testBreedDoesNotChangeChild() {
		final GeneticChooser chooser = new GeneticChooser();
		chooser.add("color", new MutatableColor());
		assertEquals(0, chooser.<MutatableColor> get("color").getRed(), 0);
		chooser.breedCurrentChild();
		assertEquals(0, chooser.<MutatableColor> get("color").getRed(), 0);
		chooser.breedCurrentChild();
		assertEquals(0, chooser.<MutatableColor> get("color").getRed(), 0);
		chooser.breedCurrentChild();
		assertEquals(0, chooser.<MutatableColor> get("color").getRed(), 0);
	}

}
