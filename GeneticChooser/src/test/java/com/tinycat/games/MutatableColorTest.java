package com.tinycat.games;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MutatableColorTest {
	@Test
	public void testSomething() {
		final MutatableColor color = new MutatableColor();
		color.getRed();
		color.getGreen();
		color.getBlue();
		color.mutate(0.5);
	}

	@Test
	public void testMutateByPoint() {
		assertEquals(0, MutatableColor.mutateByPoint(0, 1, 0), 0);
		assertEquals(1, MutatableColor.mutateByPoint(0, 1, 1), 0);
		assertEquals(.5, MutatableColor.mutateByPoint(0, 1, .5), 0);
		assertEquals(.75, MutatableColor.mutateByPoint(0, 1, .75), 0);
		assertEquals(.25, MutatableColor.mutateByPoint(0, 1, .25), 0);
		assertEquals(.75, MutatableColor.mutateByPoint(.5, 1, .5), 0);
		assertEquals(.25, MutatableColor.mutateByPoint(0, .5, .5), 0);
		assertEquals(.5, MutatableColor.mutateByPoint(1, 0, .5), 0);
	}
}
