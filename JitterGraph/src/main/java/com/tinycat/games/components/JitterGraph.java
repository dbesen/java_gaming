package com.tinycat.games.components;

import static org.lwjgl.opengl.GL11.*;

import java.util.*;

import org.lwjgl.opengl.Display;

import com.google.common.collect.Lists;
import com.tinycat.games.*;

public class JitterGraph implements GameComponent {

	private static final double graphTranspanrency = .6d;

	private final LinkedList<Long> elapsedTotalTimes = Lists.newLinkedList();
	private final LinkedList<Long> elapsedUpdateTimes = Lists.newLinkedList();
	private final LinkedList<Long> elapsedRenderTimes = Lists.newLinkedList();
	private final LinkedList<Long> elapsedSyncTimes = Lists.newLinkedList();
	private final Game myGame;

	private static final float graphHeightAdjustment = 0.5f;

	public JitterGraph(final Game game) {
		this.myGame = game;
	}

	@Override
	public void initViewMode() {
		Game.oneToOnePixelMode();
	}

	@Override
	public void render() {
		this.addElapsedTimes();
		this.renderGraph();
	}

	private void addElapsedTimes() {
		final int maxWidth = Display.getDisplayMode().getWidth();
		addWithBound(this.elapsedTotalTimes, this.myGame.getTotalTime(), maxWidth);
		addWithBound(this.elapsedUpdateTimes, this.myGame.getUpdateTime(), maxWidth);
		addWithBound(this.elapsedRenderTimes, this.myGame.getRenderTime(), maxWidth);
		addWithBound(this.elapsedSyncTimes, this.myGame.getSyncTime(), maxWidth);
	}

	private static <T> void addWithBound(final LinkedList<T> list, final T item, final int maxSize) {
		list.push(item);
		if (list.size() > maxSize)
			list.removeLast();
	}

	private void renderGraph() {
		glLineWidth(1);
		glBegin(GL_LINES);

		this.renderList(this.elapsedTotalTimes, .5, .5, .5, graphTranspanrency);
		this.renderList(this.elapsedUpdateTimes, 0, 0, 1, graphTranspanrency);
		this.renderList(this.elapsedSyncTimes, 0, 1, 0, graphTranspanrency);
		this.renderList(this.elapsedRenderTimes, 1, 0, 0, graphTranspanrency);

		glColor4d(1, 1, 1, graphTranspanrency);
		glVertex2d(0, 100 * graphHeightAdjustment);
		glVertex2d(Display.getDisplayMode().getWidth(), 100 * graphHeightAdjustment);
		glEnd();
	}

	private void renderList(final List<Long> list, final double r, final double g, final double b, final double a) {
		glColor4d(r, g, b, a);
		int xpos = 1;
		for (final Long item : list) {
			glVertex2d(xpos, 0);
			final double percent = this.calculatePercentElapsed(item);
			glVertex2d(xpos, percent * graphHeightAdjustment);
			xpos++;
		}
	}

	private double calculatePercentElapsed(final long elapsed) {
		// todo: calculate this value based on the desired frame rate from the settings
		return (elapsed / 166666.66666666666666667d);
	}

	@Override
	public void keyPressed(final int key) {
		// Nothing to process
	}

	@Override
	public void setSettingDefaults(final SettingsManager manager) {
		// Nothing to set
	}
}
