package com.tinycat.games;

import static org.lwjgl.opengl.GL11.*;

import java.util.List;

import javax.vecmath.Point3d;

import org.lwjgl.input.*;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;

import com.google.common.collect.Lists;
import com.tinycat.games.GameSettings.BooleanGameSetting;
import com.tinycat.games.GameSettings.IntGameSetting;
import com.tinycat.games.GameSettings.KeyGameSetting;
import com.tinycat.games.KeySettingsManager.KeySetting;

public abstract class Game {
	private final List<GameComponent> components = Lists.newArrayList();
	private static final String settingsFilename = "settings.txt";
	private final SettingsManager settingsManager = new SettingsManager(settingsFilename);
	private final DisplayManager displayManager = new DisplayManager(this.settingsManager);
	private long updateTime, renderTime, syncTime, totalTime;
	private static float viewAngleX, viewAngleY;
	private static float xPos, yPos, zPos;
	private boolean gameFinished = false;

	public long getUpdateTime() {
		return this.updateTime;
	}

	public long getRenderTime() {
		return this.renderTime;
	}

	public long getSyncTime() {
		return this.syncTime;
	}

	public long getTotalTime() {
		return this.totalTime;
	}

	public void startGame() {
		this.setDefaultSettings();
		this.settingsManager.readSettings();
		this.settingsManager.writeSettings();
		this.displayManager.initDisplay(this.getGameTitle());
		this.runGame();
	}

	private void runGame() {
		final int displaySyncFramerate = this.settingsManager.getIntSetting(IntGameSetting.DISPLAY_SYNC_FRAMERATE);
		while (!this.gameFinished) {
			Timer.start("mainLoop");
			Display.update();
			this.updateTime = Timer.getTimeNanoseconds("mainLoop");
			this.processInput();
			this.renderComponents();
			if (Display.isCloseRequested())
				this.gameFinished = true;
			this.renderTime = Timer.getTimeNanoseconds("mainLoop") - this.updateTime;
			Display.sync(displaySyncFramerate);
			this.totalTime = Timer.getTimeNanoseconds("mainLoop");
			this.syncTime = this.totalTime - this.updateTime - this.renderTime;
		}
	}

	private void processInput() {
		this.processViewAngle();
		this.processPlayerMovement();
		this.processKeyDownEvents();
	}

	private void processKeyDownEvents() {
		while (Keyboard.next())
			if (Keyboard.getEventKeyState()) {
				final int eventKey = Keyboard.getEventKey();
				for (final GameComponent component : this.components)
					component.keyPressed(eventKey);
			}
	}

	private void processViewAngle() {
		final int mouseSensitivity = this.settingsManager.getIntSetting(IntGameSetting.MOUSE_SENSITIVITY);
		viewAngleX += (mouseSensitivity * Mouse.getDX()) / 1000d;
		viewAngleY += (mouseSensitivity * Mouse.getDY()) / 1000d;
		if (Mouse.isGrabbed())
			Mouse.setCursorPosition(Display.getDisplayMode().getWidth() / 2, Display.getDisplayMode().getHeight() / 2);
	}

	private void processPlayerMovement() {
		final float movementScale = 0.2f;
		if (this.isKeyDown(KeyGameSetting.MOVE_FORWARD)) {
			xPos -= Math.sin(Math.toRadians(viewAngleX)) * movementScale;
			yPos += Math.cos(Math.toRadians(viewAngleX)) * movementScale;
		}
		if (this.isKeyDown(KeyGameSetting.MOVE_BACKWARD)) {
			xPos += Math.sin(Math.toRadians(viewAngleX)) * movementScale;
			yPos -= Math.cos(Math.toRadians(viewAngleX)) * movementScale;
		}
		if (this.isKeyDown(KeyGameSetting.MOVE_LEFT)) {
			xPos += Math.cos(Math.toRadians(viewAngleX)) * movementScale;
			yPos += Math.sin(Math.toRadians(viewAngleX)) * movementScale;
		}
		if (this.isKeyDown(KeyGameSetting.MOVE_RIGHT)) {
			xPos -= Math.cos(Math.toRadians(viewAngleX)) * movementScale;
			yPos -= Math.sin(Math.toRadians(viewAngleX)) * movementScale;
		}
		if (this.isKeyDown(KeyGameSetting.MOVE_UP))
			zPos -= movementScale;
		if (this.isKeyDown(KeyGameSetting.MOVE_DOWN))
			zPos += movementScale;
	}

	private boolean isKeyDown(final KeySettingsManager.KeySetting setting) {
		for (final int key : this.settingsManager.getKeySettingArray(setting))
			if (Keyboard.isKeyDown(key))
				return true;
		return false;
	}

	private void renderComponents() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		for (final GameComponent component : this.components) {
			glPushMatrix();
			// TODO: this initViewMode() call may be inappropriate here, since
			// it really only needs to be done once per component.
			// Perhaps we could save the matrices for each component and load
			// them rather than call init...
			// but is that better? The only way to tell is performance
			// testing...
			component.initViewMode();
			component.render();
			glPopMatrix();
		}
	}

	public abstract String getGameTitle();

	SettingsManager getSettingsManager() {
		return this.settingsManager;
	}

	public void setDefaultSettings() {
		this.settingsManager.setBooleanSetting(BooleanGameSetting.VSYNC, true);
		this.settingsManager.setBooleanSetting(BooleanGameSetting.FULLSCREEN, false);
		this.settingsManager.setIntSetting(IntGameSetting.WINDOWED_WIDTH, 640);
		this.settingsManager.setIntSetting(IntGameSetting.WINDOWED_HEIGHT, 480);
		this.settingsManager.setIntSetting(IntGameSetting.FULLSCREEN_WIDTH, Display.getDesktopDisplayMode().getWidth());
		this.settingsManager.setIntSetting(IntGameSetting.FULLSCREEN_HEIGHT, Display.getDesktopDisplayMode().getHeight());
		this.settingsManager.setIntSetting(IntGameSetting.FULLSCREEN_BITS_PER_PIXEL, Display.getDesktopDisplayMode()
				.getBitsPerPixel());
		this.settingsManager.setIntSetting(IntGameSetting.FULLSCREEN_FREQUENCY, Display.getDesktopDisplayMode().getFrequency());
		this.settingsManager.setIntSetting(IntGameSetting.DISPLAY_SYNC_FRAMERATE, 60);
		this.settingsManager.setIntSetting(IntGameSetting.SWAP_INTERVAL, 1);
		this.settingsManager.setIntSetting(IntGameSetting.MOUSE_SENSITIVITY, 100);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_FORWARD, Keyboard.KEY_E, Keyboard.KEY_UP);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_BACKWARD, Keyboard.KEY_D, Keyboard.KEY_DOWN);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_LEFT, Keyboard.KEY_S, Keyboard.KEY_LEFT);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_RIGHT, Keyboard.KEY_F, Keyboard.KEY_RIGHT);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_UP, Keyboard.KEY_SPACE);
		this.settingsManager.setKeySetting(KeyGameSetting.MOVE_DOWN, Keyboard.KEY_LCONTROL);

		for (final GameComponent component : this.components)
			component.setSettingDefaults(this.settingsManager);
	}

	public void addComponent(final GameComponent component) {
		this.components.add(component);
	}

	public static void oneToOnePixelMode() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getDisplayMode().getWidth(), 0, Display.getDisplayMode().getHeight(), 0, 1);
		setViewport();
	}

	public static void firstPersonMode() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective(90f, getAspectRatio(), 1f, 1000);
		// glFrustum(-10, 10, -10, 10, 1, 1000);
		setViewport();
		glRotatef(viewAngleY, 1, 0, 0);
		glRotatef(viewAngleX, 0, 1, 0);
		glTranslatef(xPos, zPos, yPos);
	}

	private static float getAspectRatio() {
		return (float) Display.getDisplayMode().getWidth() / Display.getDisplayMode().getHeight();
	}

	private static void setViewport() {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glViewport(0, 0, Display.getDisplayMode().getWidth(), Display.getDisplayMode().getHeight());
	}

	public boolean keyIs(final int key, final KeySetting nextGeneticChild) {
		return (this.settingsManager.getKeySettingCollection(nextGeneticChild).contains(key));
	}

	public void quitGame() {
		this.gameFinished = true;
	}

	public Point3d getCameraPosition() {
		return new Point3d(-xPos, -yPos, -zPos);
	}
}
