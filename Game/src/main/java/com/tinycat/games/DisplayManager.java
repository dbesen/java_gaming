package com.tinycat.games;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glEnable;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.tinycat.games.GameSettings.BooleanGameSetting;
import com.tinycat.games.GameSettings.IntGameSetting;

public class DisplayManager {
	private final SettingsManager settingsManager;

	public DisplayManager(final SettingsManager settingsManager) {
		this.settingsManager = settingsManager;
	}

	public void initDisplay(final String gameTitle) {
		try {
			initDisplayThrows(gameTitle);
		} catch (final LWJGLException e) {
			throw new RuntimeException(e);
		}
		glInit();
	}

	private void glInit() {
		// todo: should anything in here be settings?
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	private void initDisplayThrows(final String gameTitle) throws LWJGLException {
		Display.setTitle(gameTitle);
		final boolean fullscreen = settingsManager.getBooleanSetting(BooleanGameSetting.FULLSCREEN);
		DisplayMode mode;
		if (fullscreen) {
			mode = getFullscreenMode();
		} else {
			mode = getWindowedMode();
		}
		Display.setDisplayMode(mode);
		Display.setFullscreen(fullscreen);
		Display.setVSyncEnabled(settingsManager.getBooleanSetting(BooleanGameSetting.VSYNC));
		Display.setSwapInterval(settingsManager.getIntSetting(IntGameSetting.SWAP_INTERVAL));
		Display.create();
	}

	private DisplayMode getWindowedMode() {
		final int height = settingsManager.getIntSetting(IntGameSetting.WINDOWED_HEIGHT);
		final int width = settingsManager.getIntSetting(IntGameSetting.WINDOWED_WIDTH);
		final DisplayMode mode = new DisplayMode(width, height);
		return mode;
	}

	private DisplayMode getFullscreenMode() {
		final int height = settingsManager.getIntSetting(IntGameSetting.FULLSCREEN_HEIGHT);
		final int width = settingsManager.getIntSetting(IntGameSetting.FULLSCREEN_WIDTH);
		final int bpp = settingsManager.getIntSetting(IntGameSetting.FULLSCREEN_BITS_PER_PIXEL);
		final int frequency = settingsManager.getIntSetting(IntGameSetting.FULLSCREEN_FREQUENCY);
		final DisplayMode mode = getFullscreenDisplayFor(width, height, bpp, frequency);

		if (!mode.isFullscreenCapable()) {
			throw new RuntimeException("Mode is not fullscreen capable: " + mode);
		}
		return mode;
	}

	private DisplayMode getFullscreenDisplayFor(final int width, final int height, final int bpp, final int frequency) {
		DisplayMode[] displays;
		try {
			displays = Display.getAvailableDisplayModes();
		} catch (final LWJGLException e) {
			throw new RuntimeException(e);
		}
		for (final DisplayMode mode : displays) {
			if (mode.getWidth() != width)
				continue;
			if (mode.getHeight() != height)
				continue;
			if (mode.getBitsPerPixel() != bpp)
				continue;
			if (mode.getFrequency() != frequency)
				continue;
			return mode;
		}
		throw new RuntimeException("No display mode found that matches " + width + "x" + height + " " + bpp + "bpp " + frequency + "hz");
	}

}
