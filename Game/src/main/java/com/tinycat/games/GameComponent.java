package com.tinycat.games;

public interface GameComponent {
	public abstract void initViewMode();

	public abstract void render();

	public void keyPressed(int key);

	public void setSettingDefaults(SettingsManager manager);
}
