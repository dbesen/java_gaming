package com.tinycat.games;

import com.tinycat.games.BooleanSettingsManager.BooleanSetting;
import com.tinycat.games.IntSettingsManager.IntSetting;
import com.tinycat.games.KeySettingsManager.KeySetting;

public class GameSettings {

	public enum BooleanGameSetting implements BooleanSetting {
		VSYNC, FULLSCREEN
	}

	public enum IntGameSetting implements IntSetting {
		FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT, WINDOWED_WIDTH, WINDOWED_HEIGHT, DISPLAY_SYNC_FRAMERATE, FULLSCREEN_BITS_PER_PIXEL, FULLSCREEN_FREQUENCY, SWAP_INTERVAL, MOUSE_SENSITIVITY
	}

	public enum KeyGameSetting implements KeySetting {
		MOVE_FORWARD, MOVE_BACKWARD, MOVE_LEFT, MOVE_RIGHT, MOVE_UP, MOVE_DOWN
	}

}