package com.tinycat.games;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.tinycat.games.GameSettings.BooleanGameSetting;
import com.tinycat.games.GameSettings.IntGameSetting;
import com.tinycat.games.GameSettings.KeyGameSetting;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Display.class })
@SuppressStaticInitializationFor("org.lwjgl.opengl.Display")
public class GameTest {

	static class TestGame extends Game {

		@Override
		public String getGameTitle() {
			return "TestGame";
		}
	}

	@Test
	public void testSettingDefaults() {
		// todo: move all the display stuff into DisplayManager, and these display tests into DisplayManagerTest
		mockStatic(Display.class);
		final DisplayMode fakeDisplayMode = new DisplayMode(51, 41);
		when(Display.getDesktopDisplayMode()).thenReturn(fakeDisplayMode);

		final Game game = new TestGame();
		game.setDefaultSettings();
		assertEquals(true, game.getSettingsManager().getBooleanSetting(BooleanGameSetting.VSYNC));
		assertEquals(false, game.getSettingsManager().getBooleanSetting(BooleanGameSetting.FULLSCREEN));
		assertEquals(640, game.getSettingsManager().getIntSetting(IntGameSetting.WINDOWED_WIDTH));
		assertEquals(480, game.getSettingsManager().getIntSetting(IntGameSetting.WINDOWED_HEIGHT));
		assertEquals(Display.getDesktopDisplayMode().getWidth(), game.getSettingsManager().getIntSetting(IntGameSetting.FULLSCREEN_WIDTH));
		assertEquals(Display.getDesktopDisplayMode().getHeight(), game.getSettingsManager().getIntSetting(IntGameSetting.FULLSCREEN_HEIGHT));
		assertEquals(60, game.getSettingsManager().getIntSetting(IntGameSetting.DISPLAY_SYNC_FRAMERATE));
		assertEquals(Display.getDesktopDisplayMode().getBitsPerPixel(), game.getSettingsManager().getIntSetting(IntGameSetting.FULLSCREEN_BITS_PER_PIXEL));
		assertEquals(Display.getDesktopDisplayMode().getFrequency(), game.getSettingsManager().getIntSetting(IntGameSetting.FULLSCREEN_FREQUENCY));
		assertEquals(1, game.getSettingsManager().getIntSetting(IntGameSetting.SWAP_INTERVAL));
		assertEquals(100, game.getSettingsManager().getIntSetting(IntGameSetting.MOUSE_SENSITIVITY));
		assertArrayEquals(new int[] { Keyboard.KEY_E, Keyboard.KEY_UP }, game.getSettingsManager().getKeySettingArray(KeyGameSetting.MOVE_FORWARD));
		assertArrayEquals(new int[] { Keyboard.KEY_D, Keyboard.KEY_DOWN }, game.getSettingsManager().getKeySettingArray(KeyGameSetting.MOVE_BACKWARD));
		assertArrayEquals(new int[] { Keyboard.KEY_S, Keyboard.KEY_LEFT }, game.getSettingsManager().getKeySettingArray(KeyGameSetting.MOVE_LEFT));
		assertArrayEquals(new int[] { Keyboard.KEY_F, Keyboard.KEY_RIGHT }, game.getSettingsManager().getKeySettingArray(KeyGameSetting.MOVE_RIGHT));
	}
}
