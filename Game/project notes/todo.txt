Make player movement work properly in three dimensions
Move display settings out of game and into DisplayManager
Make the window resizable when windowed: http://stackoverflow.com/questions/5206775/how-do-i-make-an-lwjgl-window-resizable
Solve the question of when oneToOnePixelMode() is called
Make alt+enter toggle fullscreen (after making windowed resizable)
