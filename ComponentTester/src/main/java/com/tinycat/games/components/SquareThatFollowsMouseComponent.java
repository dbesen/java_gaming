package com.tinycat.games.components;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor3d;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;

import org.lwjgl.input.Mouse;

import com.tinycat.games.Game;
import com.tinycat.games.GameComponent;
import com.tinycat.games.SettingsManager;

public class SquareThatFollowsMouseComponent implements GameComponent {

	@Override
	public void initViewMode() {
		Game.oneToOnePixelMode();
	}

	@Override
	public void render() {
		if (!Mouse.isInsideWindow())
			return;
		glColor3d(1, 1, 1);
		glTranslatef(Mouse.getX(), Mouse.getY(), 0);
		glBegin(GL_QUADS);
		glVertex2f(-5f, -5f);
		glVertex2f(5f, -5f);
		glVertex2f(5f, 5f);
		glVertex2f(-5f, 5f);
		glEnd();
	}

	@Override
	public void keyPressed(final int key) {
		// Nothing to process
	}

	@Override
	public void setSettingDefaults(final SettingsManager manager) {
		// Nothing to set
	}

}
