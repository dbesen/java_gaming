package com.tinycat.games.components;

import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glColor3d;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex3d;

import org.lwjgl.input.Keyboard;

import com.tinycat.games.Game;
import com.tinycat.games.GameComponent;
import com.tinycat.games.GeneticChooser;
import com.tinycat.games.KeySettingsManager.KeySetting;
import com.tinycat.games.MutatableColor;
import com.tinycat.games.SettingsManager;

public class PointBlockComponent implements GameComponent {

	enum PointBlockKeySetting implements KeySetting {
		NEXT_GENETIC_CHILD, BREED_GENETIC_CHILD
	}

	private final GeneticChooser chooser = new GeneticChooser();
	private final Game game;

	@Override
	public void setSettingDefaults(final SettingsManager manager) {
		manager.setKeySetting(PointBlockKeySetting.NEXT_GENETIC_CHILD, Keyboard.KEY_N);
		manager.setKeySetting(PointBlockKeySetting.BREED_GENETIC_CHILD, Keyboard.KEY_B);
	}

	public PointBlockComponent(final Game game) {
		chooser.add("backgroundColor", new MutatableColor());
		chooser.add("cubeColor", new MutatableColor());
		this.game = game;
	}

	@Override
	public void initViewMode() {
		Game.firstPersonMode();
	}

	@Override
	public void keyPressed(final int key) {
		if (game.keyIs(key, PointBlockKeySetting.NEXT_GENETIC_CHILD)) {
			chooser.nextChild();
		}
		if (game.keyIs(key, PointBlockKeySetting.BREED_GENETIC_CHILD)) {
			chooser.breedCurrentChild();
		}
	}

	@Override
	public void render() {
		final MutatableColor background = chooser.get("backgroundColor");
		glClearColor((float) background.getRed(), (float) background.getGreen(), (float) background.getBlue(), 0f);
		final MutatableColor cube = chooser.get("cubeColor");
		glColor3d(cube.getRed(), cube.getGreen(), cube.getBlue());
		glBegin(GL_POINTS);
		for (int x = -5; x < 5; x++) {
			for (int y = -5; y < 5; y++) {
				for (int z = -5; z < 5; z++) {
					glVertex3d(x, y, z);
				}
			}
		}
		glEnd();
	}
}
