package com.tinycat.games.components;

import org.lwjgl.input.Keyboard;

import com.tinycat.games.*;
import com.tinycat.games.KeySettingsManager.KeySetting;

public class GameManagementComponent implements GameComponent {

	enum GameManagementKeySetting implements KeySetting {
		QUIT_GAME
	}

	private final Game game;

	public GameManagementComponent(final Game game) {
		this.game = game;
	}

	@Override
	public void setSettingDefaults(final SettingsManager manager) {
		manager.setKeySetting(GameManagementKeySetting.QUIT_GAME, Keyboard.KEY_ESCAPE);
	}

	@Override
	public void initViewMode() {
		// Do nothing
	}

	@Override
	public void render() {
		// Do nothing
	}

	@Override
	public void keyPressed(final int key) {
		if (this.game.keyIs(key, GameManagementKeySetting.QUIT_GAME))
			this.game.quitGame();
	}

}
