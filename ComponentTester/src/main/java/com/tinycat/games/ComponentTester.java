package com.tinycat.games;

import org.lwjgl.input.Mouse;

import com.tinycat.games.components.*;

public class ComponentTester extends Game {

	@Override
	public String getGameTitle() {
		return "ComponentTester";
	}

	public ComponentTester() {
		Mouse.setGrabbed(true);
		this.addComponent(new GameManagementComponent(this));
		// addComponent(new SquareThatFollowsMouseComponent()); // don't want
		// this while the mouse is grabbed
		// addComponent(new PointBlockComponent(this));
		this.addComponent(new FractalTerrainComponent(this));
		this.addComponent(new JitterGraph(this));
	}

	public static void main(final String[] args) {
		new ComponentTester().startGame();
	}
}
