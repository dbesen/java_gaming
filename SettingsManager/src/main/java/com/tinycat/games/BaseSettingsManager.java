package com.tinycat.games;

import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

public abstract class BaseSettingsManager<K, V> {
	protected final Map<K, V> values = Maps.newHashMap();

	public V getSetting(final K setting) {
		Preconditions.checkArgument(values.containsKey(setting));
		return values.get(setting);
	}

	public void setSetting(final K setting, final V value) {
		Preconditions.checkNotNull(setting);
		Preconditions.checkNotNull(value);
		values.put(setting, value);
	}

	Map<String, String> getDataForWriting() {
		final Map<String, String> toReturn = Maps.newHashMap();
		for (final Map.Entry<K, V> entry : values.entrySet()) {
			toReturn.put(escape(convertKeyToString(entry.getKey())), escape(convertValueToString(entry.getValue())));
		}
		return toReturn;
	}

	final void parseSettingPair(final String[] parts) {
		for (final K item : values.keySet()) {
			if (unescape(convertKeyToString(item)).equals(parts[0])) {
				try {
					values.put(item, convertValueFromString(item, unescape(parts[1])));
				} catch (final Exception e) {
					// Any exceptions thrown during conversion mean we simply ignore that line
				}
			}
		}
	}

	String convertKeyToString(final K key) {
		return key.toString();
	}

	String convertValueToString(final V value) {
		return value.toString();
	}

	abstract V convertValueFromString(K key, String value);

	private final String escape(final String string) {
		String toReturn = string;
		toReturn = toReturn.replace("\\", "\\\\");
		toReturn = toReturn.replace(":", "\\:");
		toReturn = toReturn.replace("\r", "\\r");
		toReturn = toReturn.replace("\n", "\\n");
		return toReturn;
	}

	private final String unescape(final String string) {
		String toReturn = string;
		toReturn = toReturn.replace("\\n", "\n");
		toReturn = toReturn.replace("\\r", "\r");
		toReturn = toReturn.replace("\\:", ":");
		toReturn = toReturn.replace("\\\\", "\\");
		return toReturn;
	}

}
