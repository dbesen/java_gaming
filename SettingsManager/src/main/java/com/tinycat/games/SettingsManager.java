package com.tinycat.games;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

/**
 * Class for managing settings. The basic flow is:
 * <ol>
 * <li>Set all default values with the set* methods</li>
 * <li>Call {@link #readSettings()} to read the settings from the file</li>
 * <li>Change settings with the set* methods</li>
 * <li>Call {@link #writeSettings()} to write the settings to the file</li>
 * </ol>
 * 
 */
public class SettingsManager {

	private final String myFilename;

	public SettingsManager(final String settingsFileName) {
		Preconditions.checkNotNull(settingsFileName);
		myFilename = settingsFileName;
	}

	interface Setting {
		// A setting should implement hashCode, equals, and toString. An enum is recommended.
	}

	private final IntSettingsManager intSettingsManager = new IntSettingsManager();
	private final StringSettingsManager stringSettingsManager = new StringSettingsManager();
	private final BooleanSettingsManager booleanSettingsManager = new BooleanSettingsManager();
	private final EnumSettingsManager enumSettingsManager = new EnumSettingsManager();
	private final KeySettingsManager keySettingsManager = new KeySettingsManager();

	private final BaseSettingsManager<?, ?>[] settingsManagers = { intSettingsManager, stringSettingsManager, booleanSettingsManager, enumSettingsManager, keySettingsManager };

	public void readSettings() {
		final File file = new File(myFilename);
		if (!file.exists())
			return;
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null)
				parseLine(line);
			reader.close();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	void parseLine(final String line) {
		if (line == null)
			return;
		if (!line.contains(":"))
			return;
		final String[] parts = line.split("(?<!\\\\):");
		if (parts.length == 1) {
			stringSettingsManager.parseSettingPair(new String[] { parts[0], "" });
			return;
		}
		if (parts.length != 2)
			return;
		for (final BaseSettingsManager<?, ?> settingsManager : settingsManagers) {
			settingsManager.parseSettingPair(parts);
		}
	}

	public void writeSettings() {
		final Map<String, String> dataToWrite = getDataToWrite();
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(myFilename));
			writeData(dataToWrite, writer);
			writer.close();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	TreeMap<String, String> getDataToWrite() {
		// Use a TreeMap to sort the data by key
		final TreeMap<String, String> toWrite = Maps.newTreeMap();
		for (final BaseSettingsManager<?, ?> manager : settingsManagers) {
			final Map<String, String> managerData = manager.getDataForWriting();
			checkForDuplicates(toWrite, managerData);
			toWrite.putAll(managerData);
		}
		return toWrite;
	}

	private void writeData(final Map<String, String> dataToWrite, final BufferedWriter writer) throws IOException {
		for (final Entry<String, String> entry : dataToWrite.entrySet()) {
			writer.write(entry.getKey());
			writer.write(":");
			writer.write(entry.getValue());
			writer.write(System.getProperty("line.separator"));
		}
	}

	private void checkForDuplicates(final Map<String, String> existingData, final Map<String, String> newData) {
		for (final Entry<String, String> entry : newData.entrySet()) {
			if (existingData.containsKey(entry.getKey())) {
				throw new IllegalArgumentException("Duplicate settings key: " + entry.getKey());
			}
		}
	}

	public int getIntSetting(final IntSettingsManager.IntSetting setting) {
		return intSettingsManager.getSetting(setting);
	}

	public void setIntSetting(final IntSettingsManager.IntSetting setting, final int value) {
		intSettingsManager.setSetting(setting, value);
	}

	public String getStringSetting(final StringSettingsManager.StringSetting setting) {
		return stringSettingsManager.getSetting(setting);
	}

	public void setStringSetting(final StringSettingsManager.StringSetting setting, final String value) {
		stringSettingsManager.setSetting(setting, value);
	}

	public boolean getBooleanSetting(final BooleanSettingsManager.BooleanSetting boolSetting) {
		return booleanSettingsManager.getSetting(boolSetting);
	}

	public void setBooleanSetting(final BooleanSettingsManager.BooleanSetting setting, final boolean value) {
		booleanSettingsManager.setSetting(setting, value);
	}

	public <T extends Enum<T>> T getEnumSetting(final Class<T> enumClass) {
		return enumSettingsManager.getEnumSetting(enumClass);
	}

	public <T extends Enum<T>> void setEnumSetting(final Enum<T> setting) {
		enumSettingsManager.setEnumSetting(setting);
	}

	public int[] getKeySettingArray(final KeySettingsManager.KeySetting setting) {
		return keySettingsManager.getSetting(setting);
	}

	public void setKeySetting(final KeySettingsManager.KeySetting setting, final int... values) {
		Preconditions.checkArgument(values.length > 0);
		keySettingsManager.setSetting(setting, values);
	}

	public Collection<Integer> getKeySettingCollection(final KeySettingsManager.KeySetting setting) {
		return ArrayConversion.convertToCollection(keySettingsManager.getSetting(setting));
	}

	public void setKeySetting(final KeySettingsManager.KeySetting setting, final Collection<Integer> values) {
		keySettingsManager.setSetting(setting, ArrayConversion.convertToArray(values));
	}
}
