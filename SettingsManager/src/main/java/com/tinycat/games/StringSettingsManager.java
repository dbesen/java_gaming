package com.tinycat.games;

import com.tinycat.games.SettingsManager.Setting;
import com.tinycat.games.StringSettingsManager.StringSetting;

public class StringSettingsManager extends BaseSettingsManager<StringSetting, String> {

	public interface StringSetting extends Setting {
		// A setting should implement hashCode, equals, and toString. An enum is recommended.
	}

	@Override
	String convertValueFromString(final StringSetting key, final String value) {
		return value;
	}

}
