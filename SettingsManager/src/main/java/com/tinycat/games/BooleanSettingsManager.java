package com.tinycat.games;

import com.tinycat.games.BooleanSettingsManager.BooleanSetting;
import com.tinycat.games.SettingsManager.Setting;

public class BooleanSettingsManager extends BaseSettingsManager<BooleanSetting, Boolean> {

	public interface BooleanSetting extends Setting {
		// A setting should implement hashCode, equals, and toString. An enum is recommended.
	}

	@Override
	Boolean convertValueFromString(final BooleanSetting key, final String value) {
		return parseBoolean(value.toLowerCase().trim());
	}

	private boolean parseBoolean(final String string) {
		if (string.equals("true"))
			return true;
		if (string.equals("false"))
			return false;

		if (string.equals("t"))
			return true;
		if (string.equals("f"))
			return false;

		if (string.equals("1"))
			return true;
		if (string.equals("0"))
			return false;

		if (string.equals("on"))
			return true;
		if (string.equals("off"))
			return false;

		if (string.equals("yes"))
			return true;
		if (string.equals("no"))
			return false;

		throw new IllegalArgumentException(string + " is not a boolean");
	}

}
