package com.tinycat.games;

import java.util.Set;

import org.lwjgl.input.Keyboard;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.tinycat.games.KeySettingsManager.KeySetting;
import com.tinycat.games.SettingsManager.Setting;

public class KeySettingsManager extends BaseSettingsManager<KeySetting, int[]> {
	public interface KeySetting extends Setting {
		// A setting should implement hashCode, equals, and toString. An enum is recommended.
	}

	@Override
	String convertValueToString(final int[] keyValues) {
		Preconditions.checkNotNull(keyValues);
		Preconditions.checkArgument(keyValues.length > 0);

		final Set<String> valueSet = Sets.newHashSet();
		for (final int value : keyValues) {
			valueSet.add(Keyboard.getKeyName(value));
		}

		return Joiner.on(", ").join(valueSet);

	}

	@Override
	int[] convertValueFromString(final KeySetting key, final String value) {
		final String[] parts = value.trim().split("[\\s,]+");
		final int[] toReturn = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			toReturn[i] = Keyboard.getKeyIndex(parts[i]);
		}
		return toReturn;
	}
}
