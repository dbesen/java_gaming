package com.tinycat.games;

import com.google.common.base.Preconditions;

public class EnumSettingsManager extends BaseSettingsManager<Class<?>, Enum<?>> {

	public <T extends Enum<T>> T getEnumSetting(final Class<T> enumClass) {
		Preconditions.checkNotNull(enumClass);

		// The wrong type cannot be in the map, since the map is indexed by type. So, this warning is bogus.
		@SuppressWarnings("unchecked")
		final T value = (T) values.get(enumClass);

		Preconditions.checkArgument(value != null);
		return value;
	}

	public <T extends Enum<T>> void setEnumSetting(final Enum<T> setting) {
		Preconditions.checkNotNull(setting);
		values.put(setting.getClass(), setting);
	}

	@Override
	String convertKeyToString(final Class<?> key) {
		return key.getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tinycat.games.BaseSettingsManager#convertValueFromString(java.lang.Object, java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	Enum<?> convertValueFromString(final Class<?> key, final String value) {
		// We know the types are right, so we suppress the bogus warnings above.
		// If this is a problem, we can loop over key.getEnumConstants() and compare the toString() -- but that's a worse O().
		return Enum.valueOf((Class) key, value);
	}

	@Override
	public Enum<?> getSetting(final Class<?> setting) {
		throw new RuntimeException("Not implemented for this class");
	}

	@Override
	public void setSetting(final Class<?> setting, final Enum<?> value) {
		throw new RuntimeException("Not implemented for this class");
	}

}
