package com.tinycat.games;

import com.tinycat.games.IntSettingsManager.IntSetting;
import com.tinycat.games.SettingsManager.Setting;

public class IntSettingsManager extends BaseSettingsManager<IntSetting, Integer> {
	public interface IntSetting extends Setting {
		// A setting should implement hashCode, equals, and toString. An enum is recommended.
	}

	@Override
	Integer convertValueFromString(final IntSetting key, final String value) {
		return Integer.parseInt(value.trim());
	}

}
