package com.tinycat.games;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EnumSettingsManagerTest extends SettingsManagerFileTestBase {

	private enum VsyncSetting {
		VSYNC_ON, VSYNC_OFF
	}

	private enum VsyncSettingDifferent {
		VSYNC_ON, VSYNC_OFF
	}

	@Test
	public void testVsyncOn() {
		final SettingsManager s = new SettingsManager(filename);
		s.setEnumSetting(VsyncSetting.VSYNC_ON);
		assertEquals(VsyncSetting.VSYNC_ON, s.getEnumSetting(VsyncSetting.class));
	}

	@Test
	public void testVsyncOff() {
		final SettingsManager s = new SettingsManager(filename);
		s.setEnumSetting(VsyncSetting.VSYNC_OFF);
		assertEquals(VsyncSetting.VSYNC_OFF, s.getEnumSetting(VsyncSetting.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWrongSettingType() {
		final SettingsManager s = new SettingsManager(filename);
		s.setEnumSetting(VsyncSetting.VSYNC_OFF);
		s.getEnumSetting(VsyncSettingDifferent.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotSet() {
		final SettingsManager s = new SettingsManager(filename);
		s.getEnumSetting(VsyncSetting.class);
	}

	@Test(expected = NullPointerException.class)
	public void testNullSet() {
		final SettingsManager s = new SettingsManager(filename);
		s.setEnumSetting((Enum<?>) null);
	}

	@Test(expected = NullPointerException.class)
	public void testNullGet() {
		final SettingsManager s = new SettingsManager(filename);
		s.getEnumSetting((Class<VsyncSetting>) null);
	}

	@Test
	public void testWriteRead() {
		final SettingsManager s1 = new SettingsManager(filename);
		s1.setEnumSetting(VsyncSetting.VSYNC_ON);
		s1.writeSettings();

		final SettingsManager s2 = new SettingsManager(filename);
		s2.setEnumSetting(VsyncSetting.VSYNC_OFF);
		s2.readSettings();
		assertEquals(VsyncSetting.VSYNC_ON, s2.getEnumSetting(VsyncSetting.class));
	}

	@Test
	public void testConvertKeyToString() {
		final EnumSettingsManager e = new EnumSettingsManager();
		assertEquals("VsyncSetting", e.convertKeyToString(VsyncSetting.class));
	}

	@Test
	public void testConvertKeyToStringDifferent() {
		final EnumSettingsManager e = new EnumSettingsManager();
		assertEquals("VsyncSettingDifferent", e.convertKeyToString(VsyncSettingDifferent.class));
	}

	@Test
	public void testConvertValueFromStringPerformance() {
		final EnumSettingsManager e = new EnumSettingsManager();
		assertEquals(VsyncSetting.VSYNC_ON, e.convertValueFromString(VsyncSetting.class, "VSYNC_ON"));
	}
}
