package com.tinycat.games;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tinycat.games.KeySettingsManager.KeySetting;

public class KeySettingsManagerTest extends SettingsManagerFileTestBase {

	private enum LocalKeySetting implements KeySetting {
		MOVE_FORWARD, MOVE_RIGHT
	}

	private enum LocalKeySetting2 implements KeySetting {
		MOVE_BACKWARD, MOVE_LEFT
	}

	@Test
	public void testSimple() {
		final SettingsManager s = new SettingsManager(filename);
		s.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_E);
		assertArrayEquals(new int[] { Keyboard.KEY_E }, s.getKeySettingArray(LocalKeySetting.MOVE_FORWARD));
	}

	@Test
	public void testWriteRead() {
		final SettingsManager s1 = new SettingsManager(filename);
		s1.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_E);
		s1.writeSettings();

		final SettingsManager s2 = new SettingsManager(filename);
		s2.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_W);
		s2.readSettings();

		assertArrayEquals(new int[] { Keyboard.KEY_E }, s2.getKeySettingArray(LocalKeySetting.MOVE_FORWARD));
	}

	@Test
	public void testWriteReadTwoKeys() {
		final SettingsManager s1 = new SettingsManager(filename);
		s1.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_E, Keyboard.KEY_UP);
		s1.writeSettings();

		final SettingsManager s2 = new SettingsManager(filename);
		s2.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_W, Keyboard.KEY_I);
		s2.readSettings();

		assertArrayEquals(new int[] { Keyboard.KEY_E, Keyboard.KEY_UP }, s2.getKeySettingArray(LocalKeySetting.MOVE_FORWARD));
	}

	@Test
	public void testWriteReadMoreSettings() {
		final SettingsManager s1 = new SettingsManager(filename);
		s1.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_E, Keyboard.KEY_UP);
		s1.setKeySetting(LocalKeySetting.MOVE_RIGHT, Keyboard.KEY_F, Keyboard.KEY_RIGHT);
		s1.setKeySetting(LocalKeySetting2.MOVE_BACKWARD, Keyboard.KEY_D, Keyboard.KEY_DOWN);
		s1.setKeySetting(LocalKeySetting2.MOVE_LEFT, Keyboard.KEY_S, Keyboard.KEY_LEFT);
		s1.writeSettings();

		final SettingsManager s2 = new SettingsManager(filename);
		s2.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_W, Keyboard.KEY_I);
		s2.setKeySetting(LocalKeySetting.MOVE_RIGHT, Keyboard.KEY_G, Keyboard.KEY_J);
		s2.setKeySetting(LocalKeySetting2.MOVE_BACKWARD, Keyboard.KEY_G, Keyboard.KEY_J);
		s2.setKeySetting(LocalKeySetting2.MOVE_LEFT, Keyboard.KEY_B, Keyboard.KEY_M);
		s2.readSettings();

		assertArrayEquals(new int[] { Keyboard.KEY_E, Keyboard.KEY_UP }, s2.getKeySettingArray(LocalKeySetting.MOVE_FORWARD));
		assertArrayEquals(new int[] { Keyboard.KEY_F, Keyboard.KEY_RIGHT }, s2.getKeySettingArray(LocalKeySetting.MOVE_RIGHT));
		assertArrayEquals(new int[] { Keyboard.KEY_D, Keyboard.KEY_DOWN }, s2.getKeySettingArray(LocalKeySetting2.MOVE_BACKWARD));
		assertArrayEquals(new int[] { Keyboard.KEY_S, Keyboard.KEY_LEFT }, s2.getKeySettingArray(LocalKeySetting2.MOVE_LEFT));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoKeys() {
		final SettingsManager s = new SettingsManager(filename);
		s.setKeySetting(LocalKeySetting.MOVE_FORWARD);
	}

	@Test
	public void testConvertFromString() {
		final KeySettingsManager k = new KeySettingsManager();
		final int[] values = k.convertValueFromString(LocalKeySetting.MOVE_FORWARD, "E");
		assertArrayEquals(new int[] { Keyboard.KEY_E }, values);
	}

	@Test
	public void testConvertFromStringExtraWhitespace() {
		final KeySettingsManager k = new KeySettingsManager();
		final int[] values = k.convertValueFromString(LocalKeySetting.MOVE_FORWARD, " \t E \t , \t , \t F \t ");
		assertArrayEquals(new int[] { Keyboard.KEY_E, Keyboard.KEY_F }, values);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConvertToStringEmpty() {
		final KeySettingsManager k = new KeySettingsManager();
		k.convertValueToString(new int[0]);
	}

	@Test(expected = NullPointerException.class)
	public void testConvertToStringNull() {
		final KeySettingsManager k = new KeySettingsManager();
		k.convertValueToString(null);
	}

	@Test
	public void testConvertToStringKeySetTwice() {
		final KeySettingsManager k = new KeySettingsManager();
		final String actual = k.convertValueToString(new int[] { Keyboard.KEY_S, Keyboard.KEY_S });
		assertEquals("S", actual);
	}

	@Test
	public void testBindSameKeyToTwoSettings() {
		final SettingsManager s = new SettingsManager(filename);
		s.setKeySetting(LocalKeySetting.MOVE_FORWARD, Keyboard.KEY_S);
		s.setKeySetting(LocalKeySetting.MOVE_RIGHT, Keyboard.KEY_S);
		assertArrayEquals(new int[] { Keyboard.KEY_S }, s.getKeySettingArray(LocalKeySetting.MOVE_FORWARD));
		assertArrayEquals(new int[] { Keyboard.KEY_S }, s.getKeySettingArray(LocalKeySetting.MOVE_RIGHT));
	}

	@Test
	public void testCollection() {
		final SettingsManager s = new SettingsManager(filename);
		s.setKeySetting(LocalKeySetting.MOVE_FORWARD, Lists.newArrayList(Keyboard.KEY_S));
		final Collection<Integer> result = s.getKeySettingCollection(LocalKeySetting.MOVE_FORWARD);
		assertEquals(Keyboard.KEY_S, (int) Iterables.getOnlyElement(result));
	}

}
