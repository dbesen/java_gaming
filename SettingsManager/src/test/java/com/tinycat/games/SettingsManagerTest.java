package com.tinycat.games;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Map;

import org.junit.Test;

public class SettingsManagerTest extends SettingsManagerFileTestBase {

	public enum LocalIntSetting implements IntSettingsManager.IntSetting {
		TEST_SETTING, TEST_SETTING2, DUPLICATE_SETTING
	}

	public enum LocalStringSetting implements StringSettingsManager.StringSetting {
		STRING_SETTING, DUPLICATE_SETTING
	}

	public enum LocalBooleanSetting implements BooleanSettingsManager.BooleanSetting {
		BOOL_SETTING, DUPLICATE_SETTING
	}

	@Test
	public void testConstruct() {
		new SettingsManager(filename);
	}

	@Test(expected = NullPointerException.class)
	public void testConstructWithNull() {
		new SettingsManager(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testReadUnsetIntSetting() {
		new SettingsManager(filename).getIntSetting(LocalIntSetting.TEST_SETTING);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testReadUnsetStringSetting() {
		new SettingsManager(filename).getStringSetting(LocalStringSetting.STRING_SETTING);
	}

	@Test
	public void testReadSetIntSetting() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 3);
		assertEquals(3, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testReadSetIntSettingDifferent() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 3);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 4);
		assertEquals(4, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testReadSetStringSetting() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setStringSetting(LocalStringSetting.STRING_SETTING, "asdf");
		assertEquals("asdf", sm.getStringSetting(LocalStringSetting.STRING_SETTING));
	}

	@Test
	public void testReadSetStringSettingDifferent() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setStringSetting(LocalStringSetting.STRING_SETTING, "asdf");
		sm.setStringSetting(LocalStringSetting.STRING_SETTING, "jkl");
		assertEquals("jkl", sm.getStringSetting(LocalStringSetting.STRING_SETTING));
	}

	@Test
	public void testIntValueIsPersisted() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 4);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setIntSetting(LocalIntSetting.TEST_SETTING, 1);
		sm2.readSettings();
		assertEquals(5, sm2.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testStringValueIsPersisted() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setStringSetting(LocalStringSetting.STRING_SETTING, "jkl");
		sm1.setStringSetting(LocalStringSetting.STRING_SETTING, "asdf");
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setStringSetting(LocalStringSetting.STRING_SETTING, "");
		sm2.readSettings();
		assertEquals("asdf", sm2.getStringSetting(LocalStringSetting.STRING_SETTING));
	}

	@Test
	public void testBooleanValueIsPersistedTrue() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, true);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, false);
		sm2.readSettings();
		assertTrue(sm2.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanValueIsPersistedFalse() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, false);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, true);
		sm2.readSettings();
		assertFalse(sm2.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testStringValues() {
		assertStringValue("asdf");
		assertStringValue("");
		assertStringValue(" asdf ");
		assertStringValue(" \t asdf \t ");
		assertStringValue("there is a \n in this string");
		assertStringValue("\n\n\n\n\n\n");
		assertStringValue("there is a \r in this string");
		assertStringValue("\r\r\r\r\r\r");
		assertStringValue("there is a \r\n in this string");
		assertStringValue("\r\n\r\n\r\n\r\n");
		assertStringValue("there is a \n\r in this string");
		assertStringValue("\n\r\n\r\n\r\n\r");
		assertStringValue("there is a \\ in this string");
		assertStringValue("\\\\\\");
		assertStringValue(" \\ \\ \\\\ ");
		assertStringValue("there is a : in this string");
		assertStringValue(":");
		assertStringValue(":::::::::");
	}

	private void assertStringValue(final String value) {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setStringSetting(LocalStringSetting.STRING_SETTING, value);
		assertEquals(value, sm1.getStringSetting(LocalStringSetting.STRING_SETTING));
		sm1.writeSettings();
		assertEquals(value, sm1.getStringSetting(LocalStringSetting.STRING_SETTING));

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setStringSetting(LocalStringSetting.STRING_SETTING, "default setting");
		sm2.readSettings();
		assertEquals(value, sm2.getStringSetting(LocalStringSetting.STRING_SETTING));
	}

	@Test(expected = NullPointerException.class)
	public void testStringNull() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setStringSetting(LocalStringSetting.STRING_SETTING, null);
	}

	@Test(expected = NullPointerException.class)
	public void testSetStringSettingNull() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setStringSetting(null, "asdf");
	}

	@Test(expected = NullPointerException.class)
	public void testSetIntSettingNull() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setIntSetting(null, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSettingMustHaveDefault() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 4);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.readSettings();
		sm2.getIntSetting(LocalIntSetting.TEST_SETTING);
	}

	@Test
	public void testParseLineKeyNotThere() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("key: value");
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseLineWithWhitespace() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("TEST_SETTING: \t 3 \t ");
		assertEquals(3, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseLineSameKeyTwice() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("TEST_SETTING:3");
		sm.parseLine("TEST_SETTING:4");
		assertEquals(4, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testStringSettingIsNotTrimmed() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setStringSetting(LocalStringSetting.STRING_SETTING, "jkl");
		sm.parseLine("STRING_SETTING: \t asdf \t ");
		assertEquals(" \t asdf \t ", sm.getStringSetting(LocalStringSetting.STRING_SETTING));
	}

	@Test
	public void testParseLineNoDelimiter() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("TEST_SETTING");
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseEmpty() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("");
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseNull() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine(null);
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseLineExtraDelimiter() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("TEST_SETTING:3:4");
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test
	public void testParseLineWrongType() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.parseLine("TEST_SETTING:asdf");
		assertEquals(5, sm.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	private void verifyParseLineBoolean(final String line, final boolean expectedValue) {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, !expectedValue);
		sm.parseLine("BOOL_SETTING:" + line);
		assertEquals(line, expectedValue, sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanParseLine() {
		verifyParseLineBoolean("false", false);
		verifyParseLineBoolean(" \t false \t ", false);
		verifyParseLineBoolean("FALSE", false);
		verifyParseLineBoolean("False", false);
		verifyParseLineBoolean("FaLse", false);
		verifyParseLineBoolean("f", false);
		verifyParseLineBoolean("F", false);
		verifyParseLineBoolean("off", false);
		verifyParseLineBoolean("no", false);
		verifyParseLineBoolean("0", false);
		verifyParseLineBoolean("true", true);
		verifyParseLineBoolean(" \t true \t ", true);
		verifyParseLineBoolean("TRUE", true);
		verifyParseLineBoolean("True", true);
		verifyParseLineBoolean("TrUe", true);
		verifyParseLineBoolean("t", true);
		verifyParseLineBoolean("T", true);
		verifyParseLineBoolean("on", true);
		verifyParseLineBoolean("yes", true);
		verifyParseLineBoolean("1", true);
	}

	@Test
	public void testBooleanParseLine3True() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, true);
		sm.parseLine("BOOL_SETTING:3");
		assertEquals(true, sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanParseLine3False() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, false);
		sm.parseLine("BOOL_SETTING:3");
		assertEquals(false, sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanParseLineAsdfTrue() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, true);
		sm.parseLine("BOOL_SETTING:asdf");
		assertEquals(true, sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanParseLineAsdfFalse() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, false);
		sm.parseLine("BOOL_SETTING:asdf");
		assertEquals(false, sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testReadKeyNotThere() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setIntSetting(LocalIntSetting.TEST_SETTING, 4);
		sm2.setIntSetting(LocalIntSetting.TEST_SETTING2, 3);
		sm2.readSettings();
		assertEquals(5, sm2.getIntSetting(LocalIntSetting.TEST_SETTING));
		assertEquals(3, sm2.getIntSetting(LocalIntSetting.TEST_SETTING2));
	}

	@Test
	public void testOverwriteExistingFile() {
		final SettingsManager sm1 = new SettingsManager(filename);
		sm1.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm1.writeSettings();

		final SettingsManager sm2 = new SettingsManager(filename);
		sm2.setIntSetting(LocalIntSetting.TEST_SETTING, 4);
		sm2.writeSettings();

		final SettingsManager sm3 = new SettingsManager(filename);
		sm3.setIntSetting(LocalIntSetting.TEST_SETTING, 3);
		sm3.readSettings();
		assertEquals(4, sm3.getIntSetting(LocalIntSetting.TEST_SETTING));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBooleanSettingNotThere() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING);
	}

	@Test(expected = NullPointerException.class)
	public void testSetBooleanSettingNull() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(null, false);
	}

	@Test
	public void testBooleanSettingTrue() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, true);
		assertTrue(sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test
	public void testBooleanSettingFalse() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setBooleanSetting(LocalBooleanSetting.BOOL_SETTING, false);
		assertFalse(sm.getBooleanSetting(LocalBooleanSetting.BOOL_SETTING));
	}

	@Test(expected = RuntimeException.class)
	public void testDuplicateKeys() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.DUPLICATE_SETTING, 5);
		sm.setStringSetting(LocalStringSetting.DUPLICATE_SETTING, "asdf");
		sm.writeSettings();
	}

	@Test(expected = RuntimeException.class)
	public void testDuplicateKeysBool() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.DUPLICATE_SETTING, 5);
		sm.setBooleanSetting(LocalBooleanSetting.DUPLICATE_SETTING, false);
		sm.writeSettings();
	}

	@Test
	public void testDataIsSorted() {
		final SettingsManager sm = new SettingsManager(filename);
		sm.setIntSetting(LocalIntSetting.TEST_SETTING, 5);
		sm.setStringSetting(LocalStringSetting.STRING_SETTING, "asdf");
		final Map<String, String> data = sm.getDataToWrite();
		final Iterator<String> keyIterator = data.keySet().iterator();
		assertEquals("STRING_SETTING", keyIterator.next());
		assertEquals("TEST_SETTING", keyIterator.next());
	}
}
