package com.tinycat.games;

import static org.junit.Assert.fail;

import java.io.File;

import org.junit.After;
import org.junit.Before;

public class SettingsManagerFileTestBase {

	protected final String filename = "test-settings.txt";

	@Before
	public void setUp() {
		final File file = new File(filename);
		if (file.exists()) {
			deleteFile();
			fail("File existed!");
		}
	}

	@After
	public void tearDown() {
		deleteFile();
	}

	private void deleteFile() {
		final File file = new File(filename);
		if (file.exists())
			if (!file.delete()) {
				fail("File could not be deleted!");
			}
	}
}
