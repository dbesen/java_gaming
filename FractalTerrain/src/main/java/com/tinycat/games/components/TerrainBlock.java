package com.tinycat.games.components;

import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;

import javax.vecmath.*;

import org.lwjgl.BufferUtils;

import com.google.common.base.Preconditions;
import com.tinycat.games.FractalTerrainComponent;

public class TerrainBlock {

	private final double topLeftX;
	private final double topLeftY;

	private final double size;
	private static final double minSize = 1; // so we don't render to infinity when the camera and block are at 0,0
	private static final double detailFactor = 0.05;

	private static final FloatBuffer whiteLight = BufferUtils.createFloatBuffer(4);

	static {
		whiteLight.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
	}

	public TerrainBlock(final Point2d topleft, final double size) {
		Preconditions.checkArgument(size > 0);
		this.topLeftX = topleft.x;
		this.topLeftY = topleft.y;

		this.size = size;
	}

	public TerrainBlock(final Point2d topleft, final Point2d bottomRight) {
		Preconditions.checkArgument(topleft.x < bottomRight.x);
		Preconditions.checkArgument(topleft.y < bottomRight.y);
		final double size = bottomRight.x - topleft.x;
		Preconditions.checkArgument(size == (bottomRight.y - topleft.y));
		this.topLeftX = topleft.x;
		this.topLeftY = topleft.y;

		this.size = size;
	}

	public double calcDistanceToSizeRatio(final Point3d point3d) {
		final Point3d myTopLeftPosition = this.calculateTopLeftPosition();
		return this.getSize() / myTopLeftPosition.distance(point3d);
	}

	private double getSize() {
		return this.size;
	}

	private Point3d calculateTopLeftPosition() {
		// todo: don't re-generate this point again
		// return new Point3d(this.topLeftX, this.topLeftY, FractalTerrainComponent.noiseGenerator.generate(this.topLeftX,
		// this.topLeftY, 4));
		return new Point3d(this.topLeftX, this.topLeftY, 0);
	}

	public Point2d getTopLeft() {
		return new Point2d(this.topLeftX, this.topLeftY);
	}

	public Point2d getTopRight() {
		return new Point2d(this.topLeftX + this.size, this.topLeftY);
	}

	public Point2d getBottomLeft() {
		return new Point2d(this.topLeftX, this.topLeftY + this.size);
	}

	public Point2d getBottomRight() {
		return new Point2d(this.topLeftX + this.size, this.topLeftY + this.size);
	}

	public void render(final Point3d cameraPosition) {
		this.renderLighting();
		this.renderTerrain(cameraPosition, 1);
	}

	private void renderLighting() {
		final FloatBuffer position = BufferUtils.createFloatBuffer(4);
		position.put(100.0f).put(100.0f).put(100.0f).put(0.0f).flip();
		glLight(GL_LIGHT0, GL_POSITION, position);
		glLight(GL_LIGHT0, GL_SPECULAR, whiteLight);
		glLight(GL_LIGHT0, GL_DIFFUSE, whiteLight);

		final FloatBuffer lModelAmbient = BufferUtils.createFloatBuffer(4);
		lModelAmbient.put(0.5f).put(0.5f).put(0.5f).put(1.0f).flip();
		glLightModel(GL_LIGHT_MODEL_AMBIENT, lModelAmbient);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		final FloatBuffer matSpecular = BufferUtils.createFloatBuffer(4);
		matSpecular.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();

		glMaterial(GL_FRONT, GL_SPECULAR, matSpecular);
		glMaterialf(GL_FRONT, GL_SHININESS, 50.0f);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

		glEnable(GL_COLOR_MATERIAL);
	}

	public void renderTerrain(final Point3d cameraPosition, final int currentDepth) {
		final double distanceRatio = this.calcDistanceToSizeRatio(cameraPosition);
		if ((this.size > minSize) && (distanceRatio > TerrainBlock.detailFactor))
			this.renderFourChildren(cameraPosition, currentDepth);
		else
			this.drawCurrentBlock(currentDepth);
	}

	private void drawCurrentBlock(final int currentDepth) {
		// TODO: glDrawElements()

		double topLeftX, topLeftY, topRightX, topRightY, bottomLeftX, bottomLeftY, bottomRightX, bottomRightY;

		topLeftX = bottomLeftX = this.topLeftX;
		topLeftY = topRightY = this.topLeftY;
		topRightX = bottomRightX = this.topLeftX + this.size;
		bottomLeftY = bottomRightY = this.topLeftY + this.size;

		final double heightAtTopLeft = this.getHeight(topLeftX, topLeftY, currentDepth);
		final double heightAtTopRight = this.getHeight(topRightX, topRightY, currentDepth);
		final double heightAtBottomLeft = this.getHeight(bottomLeftX, bottomLeftY, currentDepth);
		final double heightAtBottomRight = this.getHeight(bottomRightX, bottomRightY, currentDepth);

		final Vector3d vec = new Vector3d(this.size, heightAtTopRight - heightAtTopLeft, 0);
		final Vector3d vec2 = new Vector3d(0, heightAtBottomLeft - heightAtTopLeft, this.size);
		// vec.normalize();
		// vec2.normalize();
		final Vector3d normal = new Vector3d();
		normal.cross(vec2, vec);
		normal.normalize();

		// Render the normal vectors
		// todo: make this a realtime option?
		// glBegin(GL_LINES);
		// glColor4d(1, 0, 0, 1);
		// glVertex3d(topLeftX, heightAtTopLeft, topLeftY);
		// glVertex3d(topLeftX + (normal.x * 2), heightAtTopLeft + (normal.y * 2), topLeftY + (normal.z * 2));
		// glEnd();

		glBegin(GL_QUADS);
		glColor4d(1, 1, 1, 1);
		glNormal3d(normal.x, normal.y, normal.z);
		glVertex3d(topLeftX, heightAtTopLeft, topLeftY);
		glVertex3d(topRightX, heightAtTopRight, topRightY);
		glVertex3d(bottomRightX, heightAtBottomRight, bottomRightY);
		glVertex3d(bottomLeftX, heightAtBottomLeft, bottomLeftY);
		glEnd();
	}

	private double getHeight(final double x, final double y, final int currentDepth) {
		// TODO: don't re-generate points already generated
		return FractalTerrainComponent.noiseGenerator.generate(x / 16, y / 16, (currentDepth - 1) / 2) * 2;
	}

	private void renderFourChildren(final Point3d cameraPosition, final int currentDepth) {
		final double halfSize = this.size / 2;
		(new TerrainBlock(new Point2d(this.topLeftX, this.topLeftY), halfSize)).renderTerrain(cameraPosition, currentDepth + 1);
		(new TerrainBlock(new Point2d(this.topLeftX + halfSize, this.topLeftY), halfSize)).renderTerrain(cameraPosition,
				currentDepth + 1);
		(new TerrainBlock(new Point2d(this.topLeftX, this.topLeftY + halfSize), halfSize)).renderTerrain(cameraPosition,
				currentDepth + 1);
		(new TerrainBlock(new Point2d(this.topLeftX + halfSize, this.topLeftY + halfSize), halfSize)).renderTerrain(
				cameraPosition, currentDepth + 1);
	}
}
