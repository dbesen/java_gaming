package com.tinycat.games.components;

import com.tinycat.games.MathUtils;

public class ValueNoise {

	private final long shiftedSeed;

	public ValueNoise() {
		this(System.currentTimeMillis());
	}

	public ValueNoise(final long seed) {
		this.shiftedSeed = hash64shift(seed);
	}

	public double generate(final double x) {
		double total = 0;
		final double persistence = 0.5; // todo: how should these parameters be specified? in this method signature? in the
										// constructor?
		final double number_of_octaves_minus_one = 8 - 1;
		for (int i = 0; i < number_of_octaves_minus_one; i++) {
			final double frequency = MathUtils.ipow(2, i);
			final double amplitude = MathUtils.approximatePow(persistence, i);
			total += this.calculateInterpolatedNoise(x * frequency) * amplitude;
		}
		return total;
	}

	public double generate(final double x, final double y) {
		return this.generate(x, y, 4);
	}

	public double generate(final double x, final double y, final int octaves) {
		double total = 0;
		final double persistence = 0.5;
		final int number_of_octaves_minus_one = octaves - 1;
		for (int i = 0; i < number_of_octaves_minus_one; i++) {
			final double frequency = MathUtils.ipow(2, i);
			final double amplitude = MathUtils.approximatePow(persistence, i);
			total += this.calculateInterpolatedNoise(x * frequency, y * frequency) * amplitude;
		}
		return total;

	}

	public double generate(final double x, final double y, final double z) {
		// todo
		return x + y + z + this.shiftedSeed;
	}

	private double calculateInterpolatedNoise(final double x) {
		final long floor_of_x = (long) x;
		final double fractional_part_of_x = Math.abs(x - floor_of_x);

		final double v1 = this.RNG(floor_of_x);
		final double v2 = this.RNG(floor_of_x + 1);
		return MathUtils.LinearInterpolate(v1, v2, fractional_part_of_x);
	}

	private double calculateInterpolatedNoise(final double x, final double y) {
		final long floor_of_x = MathUtils.fastFloor(x);
		final double fractional_part_of_x = x - floor_of_x;
		final long floor_of_y = MathUtils.fastFloor(y);
		final double fractional_part_of_y = y - floor_of_y;

		final double v1 = this.RNG(floor_of_x, floor_of_y);
		final double v2 = this.RNG(floor_of_x + 1, floor_of_y);
		final double v3 = this.RNG(floor_of_x, floor_of_y + 1);
		final double v4 = this.RNG(floor_of_x + 1, floor_of_y + 1);

		final double i1 = MathUtils.LinearInterpolate(v1, v2, fractional_part_of_x);
		final double i2 = MathUtils.LinearInterpolate(v3, v4, fractional_part_of_x);

		return MathUtils.LinearInterpolate(i1, i2, fractional_part_of_y);
	}

	private double RNG(final long x, final long y) {
		return this.RNG(x + ValueNoise.hash64shift(y));
	}

	private double RNG(final long x) {
		return ValueNoise.hash64shift(x + this.shiftedSeed) / (double) Long.MAX_VALUE;
	}

	/*
	 * 64-bit mix function from http://www.concentric.net/~ttwang/tech/inthash.htm
	 */
	public static long hash64shift(long key) {
		key = (~key) + (key << 21); // key = (key << 21) - key - 1;
		key = key ^ (key >>> 24);
		key = (key + (key << 3)) + (key << 8); // key * 265
		key = key ^ (key >>> 14);
		key = (key + (key << 2)) + (key << 4); // key * 21
		key = key ^ (key >>> 28);
		key = key + (key << 31);
		return key;
	}
}
