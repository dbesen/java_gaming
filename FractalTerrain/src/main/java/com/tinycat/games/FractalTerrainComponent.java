package com.tinycat.games;

import static org.lwjgl.opengl.GL11.*;

import javax.vecmath.Point2d;

import com.tinycat.games.components.*;

public class FractalTerrainComponent implements GameComponent {

	private final Game game;
	public static final ValueNoise noiseGenerator = new ValueNoise(0);
	private final TerrainBlock terrainBlock = new TerrainBlock(new Point2d(-500, -500), 1000);

	public FractalTerrainComponent(final Game game) {
		this.game = game;
	}

	@Override
	public void initViewMode() {
		Game.firstPersonMode();
		// Wireframe mode
		// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // should this be Game.wireframeMode()?

		// Fill mode
		glPolygonMode(GL_FRONT, GL_FILL);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glShadeModel(GL_SMOOTH);
	}

	@Override
	public void render() {
		this.terrainBlock.render(this.game.getCameraPosition());
	}

	@Override
	public void keyPressed(final int key) {
		// Do nothing
	}

	@Override
	public void setSettingDefaults(final SettingsManager manager) {
		// Do nothing
	}

}
