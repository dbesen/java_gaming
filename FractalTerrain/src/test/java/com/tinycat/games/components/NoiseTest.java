package com.tinycat.games.components;

import static com.tinycat.games.Asserts.assertNotEquals;
import static org.junit.Assert.assertEquals;

import org.junit.*;

public class NoiseTest {

	@Test
	public void testMethodsExist() {
		final ValueNoise noise = new ValueNoise(15);
		noise.generate(3.1);
		noise.generate(3.1, 4.2);
		noise.generate(3.1, 4.2, 5.3);
	}

	@Test
	public void testOneDimensionalSameEachTime() throws Exception {
		final ValueNoise noise = new ValueNoise();
		assertEquals(noise.generate(5.4), noise.generate(5.4), 0.0d);
		assertEquals(noise.generate(3.9), noise.generate(3.9), 0.0d);
	}

	@Test
	public void testTwoDimensionalSameEachTime() throws Exception {
		final ValueNoise noise = new ValueNoise();
		assertEquals(noise.generate(5.4, 5.5), noise.generate(5.4, 5.5), 0.0d);
		assertEquals(noise.generate(3.9, 18.4), noise.generate(3.9, 18.4), 0.0d);
	}

	@Test
	public void testThreeDimensionalSameEachTime() throws Exception {
		final ValueNoise noise = new ValueNoise();
		assertEquals(noise.generate(5.4, 5.5, 6.3), noise.generate(5.4, 5.5, 6.3), 0.0d);
		assertEquals(noise.generate(3.9, 18.4, 2.2), noise.generate(3.9, 18.4, 2.2), 0.0d);
	}

	@Test
	public void testTwoDimensionalNotEitherNumber() throws Exception {
		final ValueNoise noise = new ValueNoise();
		assertNotEquals(noise.generate(5.4, 5.5), noise.generate(5.4, 5.6), 0.001d);
		assertNotEquals(noise.generate(5.4, 5.5), noise.generate(5.6, 5.5), 0.001d);
	}

	@Ignore("Disabled until implemented")
	@Test
	public void testThreeDimensionalNotAnyGivenNumber() throws Exception {
		final ValueNoise noise = new ValueNoise();
		assertNotEquals(noise.generate(5.4, 5.5, 5.6), noise.generate(5.4, 5.5, 5.7), 0.001d);
		assertNotEquals(noise.generate(5.4, 5.5, 5.6), noise.generate(5.4, 5.7, 5.6), 0.001d);
		assertNotEquals(noise.generate(5.4, 5.5, 5.6), noise.generate(5.7, 5.5, 5.6), 0.001d);
	}

	@Test
	public void testOneDSameAcrossInstancesZero() throws Exception {
		final double num1 = new ValueNoise(0).generate(0);
		final double num2 = new ValueNoise(0).generate(0);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testOneDSameAcrossInstancesOne() throws Exception {
		final double num1 = new ValueNoise(1).generate(1);
		final double num2 = new ValueNoise(1).generate(1);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testTwoDSameAcrossInstancesZero() throws Exception {
		final double num1 = new ValueNoise(0).generate(0, 0);
		final double num2 = new ValueNoise(0).generate(0, 0);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testTwoDSameAcrossInstancesOne() throws Exception {
		final double num1 = new ValueNoise(1).generate(1, 1);
		final double num2 = new ValueNoise(1).generate(1, 1);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testThreeDSameAcrossInstancesZero() throws Exception {
		final double num1 = new ValueNoise(0).generate(0, 0, 0);
		final double num2 = new ValueNoise(0).generate(0, 0, 0);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testThreeDSameAcrossInstancesOne() throws Exception {
		final double num1 = new ValueNoise(1).generate(1, 1, 1);
		final double num2 = new ValueNoise(1).generate(1, 1, 1);
		assertEquals(num1, num2, 0.0d);
	}

	@Test
	public void testSeedAffectsResultOneDZero() throws Exception {
		final double num1 = new ValueNoise(0).generate(0);
		final double num2 = new ValueNoise(1).generate(0);
		assertNotEquals(num1, num2, 0.01d);
	}

	@Test
	public void testSeedAffectsResultOneDOne() throws Exception {
		final double num1 = new ValueNoise(0).generate(1);
		final double num2 = new ValueNoise(1).generate(1);
		assertNotEquals(num1, num2, 0.01d);
	}

	@Test
	public void testSeedAffectsResultTwoD() throws Exception {
		final double num1 = new ValueNoise(0).generate(0.5, 0.5);
		final double num2 = new ValueNoise(1).generate(0.5, 0.5);
		assertNotEquals(num1, num2, 0.01d);
	}

	@Test
	public void testSeedAffectsResultThreeD() throws Exception {
		final double num1 = new ValueNoise(1).generate(0.5, 0.5, 0.5);
		final double num2 = new ValueNoise(2).generate(0.5, 0.5, 0.5);
		assertNotEquals(num1, num2, 0.01d);
	}
}
