package com.tinycat.games.components;

import static org.junit.Assert.assertEquals;

import javax.vecmath.*;

import org.junit.Test;

// to test:
// calcDistanceToSizeRatio(ThreeDPos)
// renderFourChildren...
public class TerrainBlockTest {
	@Test(expected = NullPointerException.class)
	public void testConstructNull() throws Exception {
		new TerrainBlock(null, 1);
	}

	@Test(expected = NullPointerException.class)
	public void testConstructNullExplicitFirst() throws Exception {
		new TerrainBlock(null, new Point2d(1, 1));
	}

	@Test(expected = NullPointerException.class)
	public void testConstructNullExplicitSecond() throws Exception {
		new TerrainBlock(new Point2d(1, 1), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotRectangular() throws Exception {
		new TerrainBlock(new Point2d(0, 0), new Point2d(1, 2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNegativeSize() throws Exception {
		new TerrainBlock(new Point2d(0, 0), -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testZeroSize() throws Exception {
		new TerrainBlock(new Point2d(0, 0), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNegativeSizeExplicit() throws Exception {
		new TerrainBlock(new Point2d(0, 0), new Point2d(-1, -1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSamePoint() throws Exception {
		new TerrainBlock(new Point2d(0, 0), new Point2d(0, 0));
	}

	@Test
	public void testPoint2DEquals() throws Exception {
		assertEquals(new Point2d(0, 0), new Point2d(0, 0));
		assertEquals(new Point2d(3, 3), new Point2d(3, 3));
	}

	@Test(expected = AssertionError.class)
	public void testPoint2DEqualsFail() throws Exception {
		assertEquals(new Point2d(0, 0), new Point2d(0, 1));
	}

	@Test
	public void testGetCoordsWidthHeight() throws Exception {
		final Point2d topleft = new Point2d(0, 0);
		final TerrainBlock block = new TerrainBlock(topleft, 1);
		assertEquals(new Point2d(0, 0), block.getTopLeft());
		assertEquals(new Point2d(1, 0), block.getTopRight());
		assertEquals(new Point2d(0, 1), block.getBottomLeft());
		assertEquals(new Point2d(1, 1), block.getBottomRight());
	}

	@Test
	public void testGetCoordsWidthHeightDifferent() throws Exception {
		final Point2d topleft = new Point2d(3, 3);
		final TerrainBlock block = new TerrainBlock(topleft, 1);
		assertEquals(new Point2d(3, 3), block.getTopLeft());
		assertEquals(new Point2d(4, 3), block.getTopRight());
		assertEquals(new Point2d(3, 4), block.getBottomLeft());
		assertEquals(new Point2d(4, 4), block.getBottomRight());
	}

	@Test
	public void testGetCoordsExplicit() throws Exception {
		final Point2d topleft = new Point2d(0, 0);
		final Point2d bottomRight = new Point2d(1, 1);
		final TerrainBlock block = new TerrainBlock(topleft, bottomRight);
		assertEquals(new Point2d(0, 0), block.getTopLeft());
		assertEquals(new Point2d(1, 0), block.getTopRight());
		assertEquals(new Point2d(0, 1), block.getBottomLeft());
		assertEquals(new Point2d(1, 1), block.getBottomRight());
	}

	@Test
	public void testGetCoordsExplicitDifferent() throws Exception {
		final Point2d topleft = new Point2d(3, 3);
		final Point2d bottomRight = new Point2d(4, 4);
		final TerrainBlock block = new TerrainBlock(topleft, bottomRight);
		assertEquals(new Point2d(3, 3), block.getTopLeft());
		assertEquals(new Point2d(4, 3), block.getTopRight());
		assertEquals(new Point2d(3, 4), block.getBottomLeft());
		assertEquals(new Point2d(4, 4), block.getBottomRight());
	}

	@Test
	public void testCalcDistanceToSizeRatio() {
		final TerrainBlock block = new TerrainBlock(new Point2d(0, 0), 1);
		final double result = block.calcDistanceToSizeRatio(new Point3d(1d, 0d, 0d));
		assertEquals(1, result, 0);
	}

	@Test
	public void testCalcDistanceToSizeRatioDifferent() {
		final TerrainBlock block = new TerrainBlock(new Point2d(0, 0), 1);
		final double result = block.calcDistanceToSizeRatio(new Point3d(0d, 2d, 0d));
		assertEquals(0.5, result, 0);
	}

	@Test
	public void testCalcDistanceToSizeRatioDifferentAgain() {
		final TerrainBlock block = new TerrainBlock(new Point2d(0, 0), 2);
		final double result = block.calcDistanceToSizeRatio(new Point3d(0d, 1d, 0d));
		assertEquals(2, result, 0);
	}

	@Test
	public void testPoint3dExists() throws Exception {
		new Point3d(1d, 1d, 1d);
	}

	@Test
	public void testRender() throws Exception {

	}

}
