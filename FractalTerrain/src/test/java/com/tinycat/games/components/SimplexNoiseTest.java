package com.tinycat.games.components;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tinycat.games.Asserts;

public class SimplexNoiseTest {

	@Test
	public void testGenerateP() {
		final short[] p = SimplexNoise.generateP(0);

		final boolean[] dest = new boolean[256];
		for (int i = 0; i < 256; i++) {
			final short val = p[i];
			dest[val] = true;
		}

		for (int i = 0; i < 256; i++)
			assertTrue(dest[i]);
	}

	@Test
	public void testDifferentSeedsProduceDifferentResults() {
		final short[] p1 = SimplexNoise.generateP(1);
		final short[] p2 = SimplexNoise.generateP(2);
		Asserts.assertNotEquals(p1, p2);
	}

}
